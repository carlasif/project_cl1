The Hindi dependency treebank (HDTB) is a part of an ongoing effort of creating a multi layered treebank for Hindi.  
This data is being distributed for free for non-commercial purpose only. 

The user agrees to not distribute the data or part of them either in original 
or modified form. Any publication reporting the work done using this data 
should cite the following reference: 

Martha Palmer, Rajesh Bhatt, Bhuvana Narasimhan, Owen Rambow, Dipti Misra Sharma, Fei Xia,  Hindi Syntax: Annotating Dependency, Lexical Predicate-Argument Structure, and Phrase Structure, In the Proceedings of the 7th International Conference on Natural Language Processing, ICON-2009, Hyderabad, India, Dec 14-17, 2009.

The developers of this treebank (IIIT-Hyderabad and all the other agencies involved) HDTB do not warrant the accuracy,
completeness, currentness or fitness for a particular purpose of the information 
contained in HDTB.

By downloading the data, you agree to abide by the above terms and conditions.

Disclaimer: While every care has been exercised in the development of the treebank data, we do not hold any responsibilty 
for the errors in the data.

